/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import java.net.Socket;

/**
 *
 * @author Christian
 */
public class Jugadores {

    private String tipo;
    private int numerojugador;
    private Socket sockect;

    public Socket getSockect() {
        return sockect;
    }

    public void setSockect(Socket sockect) {
        this.sockect = sockect;
    }

    public int getNumerojugador() {
        return numerojugador;
    }

    public void setNumerojugador(int numerojugador) {
        this.numerojugador = numerojugador;
    }

    public VistaPrincipal getVentana() {
        return ventana;
    }

    public void setVentana(VistaPrincipal ventana) {
        this.ventana = ventana;
    }
    private VistaPrincipal ventana;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
    private String host;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    private String nombre;
    private boolean estado;
    private String ip;
    private String usuario;

}
