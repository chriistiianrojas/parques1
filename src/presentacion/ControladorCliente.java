
package presentacion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;


public class ControladorCliente implements ActionListener{

    private VistaPrincipal ventana;
    private Modelo model;
    
    public ControladorCliente(VistaPrincipal aThis) {
        ventana = aThis;
        model = ventana.getModel();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton boton;
        if(e.getSource() instanceof JButton){
            boton = (JButton)e.getSource();
            if(boton.getName().equals("lanza")){
                try {
                    model.lanzar();
                } catch (IOException ex) {
                    Logger.getLogger(ControladorCliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
}
