package presentacion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public class ControladorServidor implements ActionListener, Runnable {

    private ServidorVentana ventana;
    private ConfigPartida unirseVentana;
    private Modelo model;
    private VistaPrincipal ventanaPrincipal;
    private Thread clienteSocket;

    public ControladorServidor(ServidorVentana aThis) {
        ventana = aThis;
        model = ventana.getModel();
    }

    public ConfigPartida getUnirseVentana() {
        if (unirseVentana == null) {
            unirseVentana = new ConfigPartida();
        }
        return unirseVentana;
    }

    public VistaPrincipal getVentanaPrincipal() {
        if (ventanaPrincipal == null) {
            ventanaPrincipal = new VistaPrincipal(model);
        }
        return ventanaPrincipal;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton boton;
        if (e.getSource() instanceof JButton) {
            boton = (JButton) e.getSource();
            System.out.println("boton.getName():" + boton.getText());
            if (boton.getText().equals("Conectarse Partida")) {
                model.iniPantalla();
            } else if (boton.getText().equals("Conectarse Partida")) {
                model.iniPartida();
            }
        }
    }

    @Override
    public void run() {
    }

}
