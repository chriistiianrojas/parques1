/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import java.awt.Canvas;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author estudiantes
 */
public class VistaPrincipal extends javax.swing.JFrame {

    private Modelo model;
    private ControladorCliente control;

    public VistaPrincipal(Modelo aThis) {
        model = aThis;
        initComponents();
        capturarEventos();
    }

    public ControladorCliente getControl() {
        if (control == null) {
            control = new ControladorCliente(this);
        }
        return control;
    }

    public Modelo getModel() {
        return model;
    }

    public Canvas getLienzo() {
        return lienzo;
    }

    public Canvas getRojo1() {
        return rojo1;
    }

    public Canvas getRojo2() {
        return rojo2;
    }

    public Canvas getDado1() {
        return dado1;
    }

    public Canvas getDado2() {
        return dado2;
    }

    public Canvas getVerde1() {
        return verde1;
    }

    public Canvas getVerde2() {
        return verde2;
    }

    public Canvas getAmarillo1() {
        return amarillo1;
    }

    public Canvas getAmarillo2() {
        return amarillo2;
    }

    public Canvas getAzul1() {
        return azul1;
    }

    public Canvas getAzul2() {
        return azul2;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rojo1 = new java.awt.Canvas();
        rojo2 = new java.awt.Canvas();
        dado1 = new java.awt.Canvas();
        dado2 = new java.awt.Canvas();
        verde1 = new java.awt.Canvas();
        verde2 = new java.awt.Canvas();
        azul1 = new java.awt.Canvas();
        lanzarDados = new javax.swing.JButton();
        azul2 = new java.awt.Canvas();
        amarillo1 = new java.awt.Canvas();
        amarillo2 = new java.awt.Canvas();
        lienzo = new java.awt.Canvas();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        rojo1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rojo1MouseClicked(evt);
            }
        });
        getContentPane().add(rojo1);
        rojo1.setBounds(80, 80, 25, 25);

        rojo2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rojo2MouseClicked(evt);
            }
        });
        getContentPane().add(rojo2);
        rojo2.setBounds(160, 80, 25, 25);
        getContentPane().add(dado1);
        dado1.setBounds(690, 40, 60, 60);
        getContentPane().add(dado2);
        dado2.setBounds(770, 40, 60, 60);

        verde1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                verde1MouseClicked(evt);
            }
        });
        getContentPane().add(verde1);
        verde1.setBounds(500, 80, 25, 25);

        verde2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                verde2MouseClicked(evt);
            }
        });
        getContentPane().add(verde2);
        verde2.setBounds(580, 80, 25, 25);

        azul1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                azul1MouseClicked(evt);
            }
        });
        getContentPane().add(azul1);
        azul1.setBounds(80, 500, 25, 25);

        lanzarDados.setText("Lanzar");
        lanzarDados.setName("lanza"); // NOI18N
        getContentPane().add(lanzarDados);
        lanzarDados.setBounds(720, 110, 85, 29);

        azul2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                azul2MouseClicked(evt);
            }
        });
        getContentPane().add(azul2);
        azul2.setBounds(160, 500, 25, 25);

        amarillo1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                amarillo1MouseClicked(evt);
            }
        });
        getContentPane().add(amarillo1);
        amarillo1.setBounds(500, 500, 25, 25);

        amarillo2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                amarillo2MouseClicked(evt);
            }
        });
        getContentPane().add(amarillo2);
        amarillo2.setBounds(580, 500, 25, 25);

        lienzo.setSize(new java.awt.Dimension(650, 650));
        getContentPane().add(lienzo);
        lienzo.setBounds(20, 10, 650, 650);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rojo1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rojo1MouseClicked
        // TODO add your handling code here:
        //System.out.println("ficha oprimida....");
        model.mover("R1");
    }//GEN-LAST:event_rojo1MouseClicked

    private void rojo2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rojo2MouseClicked
        // TODO add your handling code here:
        //System.out.println("rojo2");
        model.mover("R2");
    }//GEN-LAST:event_rojo2MouseClicked

    private void verde2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_verde2MouseClicked
        // TODO add your handling code here:
//        System.out.println("verde2");
        model.mover("V2");
    }//GEN-LAST:event_verde2MouseClicked

    private void verde1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_verde1MouseClicked
        // TODO add your handling code here:
//        System.out.println("verde1");
        model.mover("V1");
    }//GEN-LAST:event_verde1MouseClicked

    private void azul1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_azul1MouseClicked
        // TODO add your handling code here:
//        System.out.println("azul1");
        model.mover("AZ1");
    }//GEN-LAST:event_azul1MouseClicked

    private void azul2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_azul2MouseClicked
        // TODO add your handling code here:
//        System.out.println("azul2");
        model.mover("AZ2");
    }//GEN-LAST:event_azul2MouseClicked

    private void amarillo1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_amarillo1MouseClicked
        // TODO add your handling code here:
//        System.out.println("amarillo1");
        model.mover("AM1");
    }//GEN-LAST:event_amarillo1MouseClicked

    private void amarillo2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_amarillo2MouseClicked
        // TODO add your handling code here:
//        System.out.println("amarillo2");
        model.mover("AM2");
    }//GEN-LAST:event_amarillo2MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Canvas amarillo1;
    private java.awt.Canvas amarillo2;
    private java.awt.Canvas azul1;
    private java.awt.Canvas azul2;
    private java.awt.Canvas dado1;
    private java.awt.Canvas dado2;
    private javax.swing.JButton lanzarDados;
    private java.awt.Canvas lienzo;
    private java.awt.Canvas rojo1;
    private java.awt.Canvas rojo2;
    private java.awt.Canvas verde1;
    private java.awt.Canvas verde2;
    // End of variables declaration//GEN-END:variables

    private void capturarEventos() {
        lanzarDados.addActionListener(getControl());
    }
}
