package presentacion;

import dsk2.json.JSONArray;
import dsk2.json.JSONException;
import dsk2.json.JSONObject;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import logica.Sumadora;

public class Modelo implements Runnable {

    private Sumadora miSistema;
    private VistaPrincipal ventanaPrincipal;
    private ServidorVentana servidor;
    private ServidorSocket sSocket;
    private ConexionCliente conexionCliente;
    private Thread iniSocket;

    public boolean isIniPartida() {
        return iniPartida;
    }

    public void setIniPartida(boolean iniPartida) {
        this.iniPartida = iniPartida;
    }
    private Thread hiloDibujo;
    private boolean activado;
    private boolean iniPartida;
    private boolean finPartida;

    public boolean isFinPartida() {
        return finPartida;
    }

    public void setFinPartida(boolean finPartida) {
        this.finPartida = finPartida;
    }
    private Socket socket;
    private JSONArray fichas;
    private JSONArray auxfichas;
    private JSONArray tablero;
    private volatile JSONObject jugadores;
    private volatile JSONObject juego;

    public JSONArray getFichas() {
        return fichas;
    }

    public void setFichas(JSONArray fichas) {
        this.fichas = fichas;
    }

    private int puerto;
    private String host;
    private String usuario;

    public boolean isActivado() {
        return activado;
    }
    Random rand = new Random();

    public void iniciar() {
        tablero();
        setIniPartida(false);
        setFinPartida(true);
        sSocket = new ServidorSocket(this);
        System.out.println("sSocket: " + sSocket);
//        sSocket = new ServidorSocket(1234, 4);
        conexionCliente = new ConexionCliente(this);
        activado = true;
        iniPartida = false;
        finPartida = true;
        fichas = new JSONArray();
        fichas.put("AM");
        fichas.put("V");
        fichas.put("R");
        fichas.put("AZ");

        auxfichas = new JSONArray();
        auxfichas.put("AM");
        auxfichas.put("V");
        auxfichas.put("R");
        auxfichas.put("AZ");

        iniSocket = new Thread() {
            public void run() {
                System.out.println("99 Run");
                sSocket.iniServidor(1234, 4);
                System.out.println("Thread Running");
            }
        };
        iniSocket.start();
        getServidor().setSize(300, 400);
        getServidor().setVisible(true);

    }

    public void iniPantalla() {

        // Accion para el boton enviar
        //btEnviar.addActionListener(new ConexionServidor(socket, tfMensaje, usuario));
        if (auxfichas.length() > 0) {
            ventanaPrincipal = new VistaPrincipal(this);
            getVentanaPrincipal().setSize(1000, 1000);
            getVentanaPrincipal().setVisible(true);
            // Ventana de configuracion inicial
            VentanaConfiguracion vc = new VentanaConfiguracion(getVentanaPrincipal());
            host = vc.getHost();
            puerto = vc.getPuerto();
            usuario = vc.getUsuario();
            int njugador = 0;
            String nficha = "";
            if (auxfichas.length() == 4) {
                jugadores = new JSONObject();
            }
            if (auxfichas.length() < 3) {
                getServidor().getIniJuego().setEnabled(activado);
            }
            if (auxfichas.length() > 0) {
                try {
                    switch (auxfichas.length()) {
                        case 4:
                            njugador = 1;
                            break;
                        case 3:
                            njugador = 2;
                            break;
                        case 2:
                            njugador = 3;
                            break;
                        case 1:
                            njugador = 4;
                            break;
                    }
                    nficha = (String) auxfichas.remove(0);
                    socket = new Socket(host, puerto);
                    Jugadores jugador = new Jugadores();
                    jugador.setNumerojugador(njugador);
                    jugador.setNombre(nficha);
                    jugador.setUsuario(usuario);
                    jugador.setHost(host);
                    jugador.setVentana(ventanaPrincipal);
                    jugador.setSockect(socket);
                    jugador.setEstado(false);
                    jugadores.put(nficha, jugador);
                    System.out.println("jugadores:" + jugadores.toString(1));
                    hiloDibujo = new Thread() {
                        public void run() {
                            while (activado) {
                                try {
                                    dibujar(ventanaPrincipal);
                                } catch (IOException ex) {
                                    System.out.println("189 Modelo ex:" + ex.getMessage());
                                }
                            }
                            System.out.println("Thread Running");
                        }
                    };
                    hiloDibujo.start();
                } catch (IOException ex) {
                    Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
                } catch (JSONException ex) {
                    Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

        //hiloDibujo = new Thread(this);
        //hiloDibujo.start();
    }

    public void iniPartida() {
        try {
            setIniPartida(true);
            Jugadores jugador;
            NotificacionServidor notificacion;
            JSONObject infoJugador;
            while (finPartida) {
                for (int i = 0; i < getFichas().length(); i++) {
                    jugador = (Jugadores) jugadores.get(getFichas().getString(i));
                    jugador.setEstado(activado);
                    infoJugador = new JSONObject();
                    infoJugador.put("nombre", jugador.getNombre());
                    infoJugador.put("tipo", jugador.getTipo());
                    infoJugador.put("ventanaprincipal", jugador.getVentana());
                    infoJugador.put("usuario", jugador.getUsuario());
                    notificacion = new NotificacionServidor();
                    notificacion.setMensaje(infoJugador.toString());
                    ConexionCliente cc = new ConexionCliente(jugador.getSockect(), notificacion);
                    cc.start();
                }

            }
        } catch (JSONException ex) {
            Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Sumadora getMiSistema() {
        if (miSistema == null) {
            miSistema = new Sumadora();
        }
        return miSistema;
    }

    public VistaPrincipal getVentanaPrincipal() {
        if (ventanaPrincipal == null) {
            ventanaPrincipal = new VistaPrincipal(this);
        }
        return ventanaPrincipal;
    }

    public ServidorVentana getServidor() {
        if (servidor == null) {
            servidor = new ServidorVentana(this);
        }
        return servidor;
    }

    public void mover(String ficha) {
        System.out.println("la ficha a mover es: " + ficha);
        if (ficha.equals("R1")) {
            int posX = getVentanaPrincipal().getRojo1().getX();
            int posY = getVentanaPrincipal().getRojo1().getY();
            System.out.println("posX: " + posX);
            System.out.println("posY: " + posY);
            getVentanaPrincipal().getRojo1().setLocation(375, 630);
        }

    }

    public void lanzar() throws IOException {
        System.out.println("lanzar dados....");
        int d1 = (int) (Math.random() * 6 + 1);
        int d2 = (int) (Math.random() * 6 + 1);
        System.out.println("el dado 1 es: " + d1);
        System.out.println("el dado 2 es: " + d2);
        Image uno = ImageIO.read(getClass().getResource("1.jpeg"));
        Image dos = ImageIO.read(getClass().getResource("2.jpeg"));
        Image tres = ImageIO.read(getClass().getResource("3.jpeg"));
        Image cuatro = ImageIO.read(getClass().getResource("4.jpeg"));
        Image cinco = ImageIO.read(getClass().getResource("5.jpeg"));
        Image seis = ImageIO.read(getClass().getResource("6.jpeg"));

        Graphics dado1 = getVentanaPrincipal().getDado1().getGraphics();
        Graphics dado2 = getVentanaPrincipal().getDado2().getGraphics();
        switch (d1) {
            case 1:
                dado1.drawImage(uno, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            case 2:
                dado1.drawImage(dos, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            case 3:
                dado1.drawImage(tres, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            case 4:
                dado1.drawImage(cuatro, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            case 5:
                dado1.drawImage(cinco, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            case 6:
                dado1.drawImage(seis, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            default:
                break;
        }

        switch (d2) {
            case 1:
                dado2.drawImage(uno, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            case 2:
                dado2.drawImage(dos, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            case 3:
                dado2.drawImage(tres, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            case 4:
                dado2.drawImage(cuatro, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            case 5:
                dado2.drawImage(cinco, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            case 6:
                dado2.drawImage(seis, 0, 0, 60, 60, null, ventanaPrincipal);
                break;
            default:
                break;
        }

    }

    public void dibujar() throws IOException {
        Image back = ImageIO.read(getClass().getResource("tablero.jpg"));
        Image rojo1 = ImageIO.read(getClass().getResource("rojo1.png"));
        Image rojo2 = ImageIO.read(getClass().getResource("rojo2.png"));
        Image verde1 = ImageIO.read(getClass().getResource("verde1.png"));
        Image verde2 = ImageIO.read(getClass().getResource("verde2.png"));
        Image azul1 = ImageIO.read(getClass().getResource("azul1.png"));
        Image azul2 = ImageIO.read(getClass().getResource("azul2.png"));
        Image amarillo1 = ImageIO.read(getClass().getResource("amarillo1.png"));
        Image amarillo2 = ImageIO.read(getClass().getResource("amarillo2.png"));

        Graphics tablero = getVentanaPrincipal().getLienzo().getGraphics();
        Graphics fichaRojo1 = getVentanaPrincipal().getRojo1().getGraphics();
        Graphics fichaRojo2 = getVentanaPrincipal().getRojo2().getGraphics();
        Graphics fichaVerde1 = getVentanaPrincipal().getVerde1().getGraphics();
        Graphics fichaVerde2 = getVentanaPrincipal().getVerde2().getGraphics();
        Graphics fichaAzul1 = getVentanaPrincipal().getAzul1().getGraphics();
        Graphics fichaAzul2 = getVentanaPrincipal().getAzul2().getGraphics();
        Graphics fichaAmarillo1 = getVentanaPrincipal().getAmarillo1().getGraphics();
        Graphics fichaAmarillo2 = getVentanaPrincipal().getAmarillo2().getGraphics();

        tablero.drawImage(back, 0, 0, 650, 650, null, ventanaPrincipal);
        fichaRojo1.drawImage(rojo1, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaRojo2.drawImage(rojo2, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaVerde1.drawImage(verde1, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaVerde2.drawImage(verde2, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaAzul1.drawImage(azul1, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaAzul2.drawImage(azul2, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaAmarillo1.drawImage(amarillo1, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaAmarillo2.drawImage(amarillo2, 0, 0, 25, 25, null, ventanaPrincipal);
    }

    public void dibujar(VistaPrincipal ventanaPrincipal) throws IOException {
        Image back = ImageIO.read(getClass().getResource("tablero.jpg"));
        Image rojo1 = ImageIO.read(getClass().getResource("rojo1.png"));
        Image rojo2 = ImageIO.read(getClass().getResource("rojo2.png"));
        Image verde1 = ImageIO.read(getClass().getResource("verde1.png"));
        Image verde2 = ImageIO.read(getClass().getResource("verde2.png"));
        Image azul1 = ImageIO.read(getClass().getResource("azul1.png"));
        Image azul2 = ImageIO.read(getClass().getResource("azul2.png"));
        Image amarillo1 = ImageIO.read(getClass().getResource("amarillo1.png"));
        Image amarillo2 = ImageIO.read(getClass().getResource("amarillo2.png"));

        Graphics tablero = ventanaPrincipal.getLienzo().getGraphics();
        Graphics fichaRojo1 = ventanaPrincipal.getRojo1().getGraphics();
        Graphics fichaRojo2 = ventanaPrincipal.getRojo2().getGraphics();
        Graphics fichaVerde1 = ventanaPrincipal.getVerde1().getGraphics();
        Graphics fichaVerde2 = ventanaPrincipal.getVerde2().getGraphics();
        Graphics fichaAzul1 = ventanaPrincipal.getAzul1().getGraphics();
        Graphics fichaAzul2 = ventanaPrincipal.getAzul2().getGraphics();
        Graphics fichaAmarillo1 = ventanaPrincipal.getAmarillo1().getGraphics();
        Graphics fichaAmarillo2 = ventanaPrincipal.getAmarillo2().getGraphics();

        tablero.drawImage(back, 0, 0, 650, 650, null, ventanaPrincipal);
        fichaRojo1.drawImage(rojo1, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaRojo2.drawImage(rojo2, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaVerde1.drawImage(verde1, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaVerde2.drawImage(verde2, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaAzul1.drawImage(azul1, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaAzul2.drawImage(azul2, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaAmarillo1.drawImage(amarillo1, 0, 0, 25, 25, null, ventanaPrincipal);
        fichaAmarillo2.drawImage(amarillo2, 0, 0, 25, 25, null, ventanaPrincipal);
    }

    @Override
    public void run() {

        try {
            if (getFichas().length() > 0) {
                ventanaPrincipal = new VistaPrincipal(this);
                getVentanaPrincipal().setSize(1000, 1000);
                getVentanaPrincipal().setVisible(true);
                // Ventana de configuracion inicial
                VentanaConfiguracion vc = new VentanaConfiguracion(getVentanaPrincipal());
                host = vc.getHost();
                puerto = vc.getPuerto();
                usuario = vc.getUsuario();
                int njugador = 0;
                String nficha = "";
                if (getFichas().length() == 4) {
                    jugadores = new JSONObject();
                }
                if (getFichas().length() > 0) {
                    switch (getFichas().length()) {
                        case 4:
                            njugador = 1;
                            break;
                        case 3:
                            njugador = 2;
                            break;
                        case 2:
                            njugador = 3;
                            break;
                        case 1:
                            njugador = 4;
                            break;
                    }
                    nficha = (String) getFichas().remove(0);
                    socket = new Socket(host, puerto);
                    Jugadores jugador = new Jugadores();
                    jugador.setNumerojugador(njugador);
                    jugador.setNombre(nficha);
                    jugador.setUsuario(usuario);
                    jugador.setHost(host);
                    jugador.setVentana(ventanaPrincipal);
                    jugador.setSockect(socket);
                    jugador.setEstado(false);
                    jugadores.put(nficha, jugador);
                    System.out.println("jugadores:" + jugadores.toString(1));

                }
            }
        } catch (UnknownHostException ex) {
            System.out.println("UnknownHostException No se ha podido conectar con el servidor (" + ex.getMessage() + ").");
        } catch (IOException ex) {
            System.out.println("IOException No se ha podido conectar con el servidor (" + ex.getMessage() + ").");
        } catch (JSONException ex) {
            System.out.println("JSONException No se ha podido conectar con el servidor (" + ex.getMessage() + ").");
        }
        while (activado) {
            try {
                dibujar();
            } catch (IOException ex) {
                System.out.println("189 Modelo ex:" + ex.getMessage());
            }
        }
    }

    private void tablero() {
        FileInputStream archivo = null;
        try {
            archivo = new FileInputStream("tablero.properties");
            Properties prop = new Properties();
            prop.load(archivo);
            archivo.close();
            String info = "", rojo = "", verde = "", amarillo = "", azul = "";
            String[] array, array2;
            tablero = new JSONArray();
            JSONObject aux;
            for (int i = 1; i <= 68; i++) {
                info = prop.getProperty("tablero." + (i));
                array = info.split(",");
                if (array.length > 0) {
                    aux = new JSONObject();
                    aux.put("x", array[0]);
                    aux.put("y", array[1]);
                    aux.put("f_habil", array[2]);
                    aux.put("pos", i);
                    if (aux != null) {
                        tablero.put(i, aux);
                    }
                }
            }
            juego = new JSONObject();
            String[] colores = {"rojo", "amarillo", "verde", "azul"};
            for (int j = 0; j < colores.length; j++) {
                String colore = colores[j];
                for (int i = 1; i <= 2; i++) {
                    array2 = prop.getProperty(colore + (i)).split(",");
                    aux = new JSONObject();
                    aux.put("x", array2[0]);
                    aux.put("y", array2[1]);
                    aux.put("pos_ini", array2[2]);
                    aux.put("pos_fin", array2[3]);
                    juego.put(colore + i, aux);
                }
            }

            aux = new JSONObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                archivo.close();
            } catch (IOException ex) {
                Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
