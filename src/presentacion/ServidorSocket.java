/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import dsk2.json.JSONObject;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Christian
 */
public class ServidorSocket {

    private Modelo model;
    private ControladorCliente control;
    int puerto;
    int maximoConexiones; // Maximo de conexiones simultaneas

    public ServidorSocket(Modelo model) {
        if (model == null) {
            this.model = model;
        }
    }

    public void iniServidor(int puerto, int maximoConexiones) {

        ServerSocket servidor = null;
        Socket socket = null;
        NotificacionServidor notificacion = new NotificacionServidor();
        try {
            // Se crea el serverSocket
            servidor = new ServerSocket(puerto, maximoConexiones);
            // Bucle infinito para esperar conexiones
            //System.out.println("model.isIniPartida():" + model.isIniPartida());
            while (true) {
//                if (!model.isIniPartida()) {
                System.out.println("Servidor a la espera de conexiones.");
                socket = servidor.accept();
                System.out.println("Cliente con la IP " + socket.getInetAddress().getHostName() + " conectado.");
                ConexionCliente cc = new ConexionCliente(socket, notificacion);
                cc.start();
//                }
            }
        } catch (IOException ex) {
            System.out.println("37 IOException ex: " + ex.getMessage());
        } finally {
            try {
                socket.close();
                servidor.close();
            } catch (IOException ex) {
                System.out.println("43 IOException ex: " + ex.getMessage());
            }
        }
    }
}
